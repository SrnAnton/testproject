using Avalonia;
using Avalonia.Markup.Xaml.Styling;

namespace Theme;

public sealed class ThemeService
{
    private static readonly StyleInclude DefaultStyle = CreateStyle("avares://Theme/Themes/Default.axaml");
    private static readonly StyleInclude SimpleStyle = CreateStyle("avares://Theme/Themes/Simple.axaml");
    private readonly Queue<StyleInclude> _styles = new();
    private readonly Application _application;
    public ThemeService()
    {
        _application = Application.Current!;
      
        _styles.Enqueue(DefaultStyle);
        _styles.Enqueue(SimpleStyle);
    }

    private static StyleInclude CreateStyle(string url)
    {
        var self = new Uri("resm:Styles?assembly=Theme");
        return new StyleInclude(self)
        {
            Source = new Uri(url),
        };
    }


    public void NextTheme()
    {
        var style = _styles.Dequeue();
        _application.Styles[0] = style;
        
        _styles.Enqueue(style);
    }
}