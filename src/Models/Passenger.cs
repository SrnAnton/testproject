namespace Models;

[Serializable]
public class Passenger(string name, string middleName, string surname)
{
    public string Name { get; init; } = name;
    public string MiddleName { get; init; } = middleName;
    public string Surname { get; init; } = surname;
}