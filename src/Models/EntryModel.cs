namespace Models;

[Serializable]
public class EntryModel
{
    public List<Passenger> Passengers { get; } = [];
    public string Flight  { get; init; } = string.Empty;
    public DateTime DepartureTime { get; init; } = DateTime.Now;
}