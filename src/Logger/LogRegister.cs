using System.Runtime.CompilerServices;
using Serilog;
using Serilog.Context;


namespace Logger;

public class LogRegister
{
    public static void Debug(string message, Exception? e = null, [CallerMemberName] string method = "", [CallerFilePath] string sourceFilePath = "") 
        => SendLog(Log.Debug, message, e, method, sourceFilePath);

    public static void Information(string message, Exception? e = null, [CallerMemberName] string method = "", [CallerFilePath] string sourceFilePath = "")
        => SendLog(Log.Information, message, e, method, sourceFilePath);
    
    public static void Warning(string message, Exception? e = null, [CallerMemberName] string method = "", [CallerFilePath] string sourceFilePath = "")
        => SendLog(Log.Warning, message, e, method, sourceFilePath);
    
    public static void Error(string message, Exception? e = null, [CallerMemberName] string method = "", [CallerFilePath] string sourceFilePath = "")
        => SendLog(Log.Error, message, e, method, sourceFilePath);
    
    public static void Fatal(string message, Exception? e = null, [CallerMemberName] string method = "", [CallerFilePath] string sourceFilePath = "")
        => SendLog(Log.Fatal, message, e, method, sourceFilePath);

    public void Close() => Log.CloseAndFlush();
    
    private static void SendLog(Action<Exception?,string> log, string message, Exception? e, string method, string sourceFilePath)
    {
        var sourceFile = Path.GetFileNameWithoutExtension(sourceFilePath);
        var errorMessage = "";

        if (e != null)
            errorMessage = e.Message + " - " + e.StackTrace;

        using (LogContext.PushProperty("Method", method))
        using (LogContext.PushProperty("Except", errorMessage))
        using (LogContext.PushProperty("Source", sourceFile))
            log.Invoke(e, message);
    }
}