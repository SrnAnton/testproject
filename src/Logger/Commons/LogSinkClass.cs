using Serilog.Core;
using Serilog.Events;

namespace Logger.Commons;

internal class LogSinkClass(Action<string>? emiter = null) : ILogEventSink
{
    public void Emit(LogEvent logEvent)
    {
        var log = logEvent.ToLog().ToString();
        
        emiter?.Invoke(log);
    }
}