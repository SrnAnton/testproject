using Serilog;
using Serilog.Configuration;
using Serilog.Events;

namespace Logger.Commons;

public static class LoggerExtensions
{
    public static LoggerConfiguration LogSink(this LoggerSinkConfiguration loggerConfiguration, Action<string>? emiter)
        => loggerConfiguration.Sink(new LogSinkClass(emiter));

    public static LoggerConfiguration LogFile(this LoggerSinkConfiguration loggerConfiguration, string path = "", int limitFiles = 10)
        => loggerConfiguration.Sink(new LoggerFile(path, limitFiles));
    
    internal static LogInfo ToLog(this LogEvent logEvent)
    {
        return new LogInfo(
            DateTimeOffset.Now,
            logEvent.Level.ToString(),
            logEvent.Properties.TryGetValue("Source", out var sourceValue)
                ? sourceValue.ToString().Replace("\"", "")
                : string.Empty,
            logEvent.Properties.TryGetValue("Method", out var methodValue)
                ? methodValue.ToString().Replace("\"", "")
                : string.Empty,
            logEvent.MessageTemplate.ToString(),
            logEvent.Properties.TryGetValue("Except", out var exceptValue)
                ? exceptValue.ToString().Replace("\"", "")
                : string.Empty);
    }
}