using System.Globalization;
using Serilog.Core;
using Serilog.Events;

namespace Logger.Commons;

public class LoggerFile : ILogEventSink
{
    private readonly Mutex _mutex = new();
    
    private readonly int _limitFiles;
    private readonly string _path;
    private readonly string _filePath;
    
    public LoggerFile(string path, int limitFiles)
    {
        _path = path;
        _limitFiles = limitFiles;
        
        Directory.CreateDirectory(path);
        
        var nameFile = DateTime.Now.ToString(LoggerConst.DateFileFormat) + ".txt";
        _filePath = path + "/" + nameFile;
        using var fs = File.Create(_filePath);
        
        CheckFiles();
    }
    
    public void Emit(LogEvent logEvent)
    {
        _mutex.WaitOne();

        try
        {
            var log = logEvent.ToLog().ToString();
            
            using var writer = new StreamWriter(_filePath, true);
            writer.WriteLine(log);
            writer.Flush();
        }
        catch (Exception)
        {
            // ignored
        }
        
        _mutex.ReleaseMutex();
    }

    private void CheckFiles()
    {
        var files = Directory.GetFiles(_path);
        var filesName = GetFileNames(_path);
        
        if (filesName.Count <= _limitFiles) 
            return;
        
        var dates = GetOldDates(filesName, _limitFiles).Select(x => x.ToString(LoggerConst.DateFileFormat));
        foreach (var dateFile in dates)
        {
            var path = files.FirstOrDefault(x => Path.GetFileNameWithoutExtension(x) == dateFile)!;
            File.Delete(path);
        }
    }

    private List<string> GetFileNames(string path)
    {
        var files = Directory.GetFiles(path);
        return files.Select(Path.GetFileNameWithoutExtension).ToList()!;
    }

    private IEnumerable<DateTime> GetOldDates(IEnumerable<string> dateStrings, int count)
    {
        return dateStrings
            .Select(dateString => DateTime.TryParseExact(
                dateString, 
                LoggerConst.DateFileFormat, 
                CultureInfo.InvariantCulture, 
                DateTimeStyles.None, 
                out var parsedDate) 
                ? parsedDate 
                : DateTime.MinValue)
            .OrderByDescending(date => date)
            .Skip(count)
            .ToList();
    }
}