using System.Text;

namespace Logger.Commons;

internal class LogInfo(DateTimeOffset time, string level, string source, string method, string message, string exception)
{
    public DateTimeOffset Time { get; } = time;
    public string Level { get; } = level;
    public string Source { get; } = source;
    public string Method { get; } = method;
    public string Message { get; } = message;
    public string Exception { get; } = exception;

    public override string ToString()
    {
        var formattedLog = new StringBuilder();
        formattedLog.Append($"[{Time.ToString("dd.MM.yyyy HH:mm:ss")}] [{Level}] [{Source}] [{Method}] [{Message}] [{Exception}]");
        return formattedLog.ToString();
    }
}