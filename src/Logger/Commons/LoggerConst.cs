namespace Logger.Commons;

public abstract class LoggerConst
{
    public const string DateFileFormat = "dd.MM.yyyy HH-mm-ss";
    public const string Format =
        "[{Timestamp:dd.MM.yyyy HH:mm:ss}] [{Level}] [{Source}] [{Method}] [{Message}] [{Except}]{NewLine}";
}