using Avalonia.Controls.Notifications;

namespace Notification.Interfaces;

public interface INotificationHost
{
    public INotificationManager GetNotificationManager();
}