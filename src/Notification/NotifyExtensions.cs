using Avalonia.Controls;
using Avalonia.Controls.Notifications;
using Notification.Interfaces;

namespace Notification;

internal static class NotifyExtensions
{
    public static void Notify(this Window? window, INotification notification)
    {
        if (window is INotificationHost manager)
        {
            manager.GetNotificationManager().Show(notification);
        }
    }
}