using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Notifications;

namespace Notification;

public static class NotifyRegister
{
    private static Window GetActiveWindow(this IEnumerable<Window> sender)
    {
        var enumerable = sender as Window[] ?? sender.ToArray();
        var activeWindow = enumerable.FirstOrDefault(window => window.IsFocused || window.IsActive)
                           ?? enumerable.Last();

        return activeWindow ?? throw new InvalidOperationException();
    }

    private static bool TryGetWindow(out Window? window)
    {
        window = null;
        try
        {
            if (Application.Current!.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                window = desktop.Windows.GetActiveWindow();
                return true;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return false;
    }

    public static void Success(string message = "", string title = "")
    {
        if (TryGetWindow(out var window))
            window.Notify(new Avalonia.Controls.Notifications.Notification(message, title, NotificationType.Success));
    }

    public static void Warning(string message = "", string title = "")
    {
        if (TryGetWindow(out var window))
            window.Notify(new Avalonia.Controls.Notifications.Notification(message, title, NotificationType.Warning));
    }

    public static void Error(string message = "", string title = "")
    {
        if (TryGetWindow(out var window))
            window.Notify(new Avalonia.Controls.Notifications.Notification(message, title, NotificationType.Error));
    }

    public static void Information(string message = "", string title = "")
    {
        if (TryGetWindow(out var window))
            window.Notify(new Avalonia.Controls.Notifications.Notification(message, title));
    }
}