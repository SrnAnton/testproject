using Avalonia.Controls;
using Avalonia.Controls.Notifications;
using Notification.Interfaces;

namespace Notification;

public abstract class CustomizedWindow : Window, INotificationHost
{
    private INotificationManager? _notificationManager;

    public INotificationManager GetNotificationManager()
        => _notificationManager ??= new WindowNotificationManager(this)
            { Position = NotificationPosition.BottomRight, MaxItems = 3 };
} 