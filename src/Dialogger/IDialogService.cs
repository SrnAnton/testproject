using Dialogger.Commons;

namespace Dialogger;

public interface IDialogService
{
    public void ShowDialog(string viewModelName)
        => ShowDialog(viewModelName, new NavigationParameterBase());

    public async Task ShowDialogAsync(string viewModelName)
        => await ShowDialogAsync<DialogResultBase>(viewModelName);

    public async Task ShowDialogAsync<TResult>(string viewModelName) where TResult : DialogResultBase
        => await ShowDialogAsync<TResult, NavigationParameterBase>(viewModelName, new NavigationParameterBase());

    public async Task ShowDialogAsync<TParameter>(string viewModelName, TParameter parameter)
        where TParameter : NavigationParameterBase
        => await ShowDialogAsync<DialogResultBase, TParameter>(viewModelName, parameter);

    public void ShowDialog<TParameter>(string viewModelName, TParameter parameter)
        where TParameter : NavigationParameterBase;

    public Task<TResult> ShowDialogAsync<TResult, TParameter>(string viewModelName, TParameter parameter)
        where TResult : DialogResultBase
        where TParameter : NavigationParameterBase;
}