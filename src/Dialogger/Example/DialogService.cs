
//     public async Task<Stream?> ShowOpenFileDialog(FilePickerOpenOptions? options = null)
//     {
//         options ??= new FilePickerOpenOptions();
//
//         IReadOnlyList<IStorageFile> result =
//             await _windowProvider.GetActiveWindow().StorageProvider.OpenFilePickerAsync(options);
//
//         if (result.Count == 0) return Stream.Null;
//
//         return await result[0].OpenReadAsync();
//     }
//
//     public async Task<IReadOnlyList<IStorageFile>?> ShowOpenMultipleFileDialog(
//         FilePickerOpenOptions? options = null,
//         bool openFromApplicationDirectory = false
//     )
//     {
//         options ??= new FilePickerOpenOptions { AllowMultiple = true, };
//
//         var storageProvider = _windowProvider.GetActiveWindow().StorageProvider;
//
//         if (openFromApplicationDirectory)
//         {
//             options.SuggestedStartLocation =
//                 await storageProvider.TryGetFolderFromPathAsync(AppDomain.CurrentDomain.BaseDirectory);
//         }
//
//         return await storageProvider.OpenFilePickerAsync(options);
//     }