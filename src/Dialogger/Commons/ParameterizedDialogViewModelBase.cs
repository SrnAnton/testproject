namespace Dialogger.Commons;

public abstract class ParameterizedDialogViewModelBase<TResult, TParameter> : DialogViewModelBase<TResult>
    where TResult : DialogResultBase
    where TParameter : NavigationParameterBase
{
    public abstract void Activate(TParameter parameter);
}

public abstract class ParameterizedDialogViewModelBase<TParameter> :
    ParameterizedDialogViewModelBase<DialogResultBase, TParameter>
    where TParameter : NavigationParameterBase;

public abstract class
    ParameterizedDialogViewModelBaseAsync<TParameter> : ParameterizedDialogViewModelBaseAsync<DialogResultBase,
    TParameter>
    where TParameter : NavigationParameterBase;

public abstract class ParameterizedDialogViewModelBaseAsync<TResult, TParameter> : DialogViewModelBase<TResult>
    where TResult : DialogResultBase
    where TParameter : NavigationParameterBase
{
    public abstract Task ActivateAsync(TParameter parameter, CancellationToken cancellationToken = default);
}