using System.Windows.Input;
using Commons.Commons;
using ReactiveUI;

namespace Dialogger.Commons;

public class DialogViewModelBase : DialogViewModelBase<DialogResultBase>;


public class DialogViewModelBase<TResult> : ViewModelBase
    where TResult : DialogResultBase
{
    public event EventHandler<DialogResultEventArgs<TResult>> CloseRequested;

    public ICommand CloseCommand { get; }

    protected DialogViewModelBase()
    {
        CloseCommand = ReactiveCommand.Create(Close);
    }

    protected void Close() => Close(default);

    protected void Close(TResult result)
    {
        var args = new DialogResultEventArgs<TResult>(result);

        CloseRequested.Raise(this, args);
    }
}

public class DialogResultEventArgs<TResult> : EventArgs
{
    public TResult Result { get; }

    public DialogResultEventArgs(TResult result)
    {
        Result = result;
    }
}

public static class EventExtensions
{
    public static void Raise<TEventArgs>(
        this EventHandler<TEventArgs>? eventHandler,
        object sender,
        TEventArgs args) where TEventArgs : EventArgs
    {
        var handler = Volatile.Read(ref eventHandler);

        handler?.Invoke(sender, args);
    }
}

