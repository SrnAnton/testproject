using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;

namespace Dialogger.Commons;

internal static class Extensions
{
    public static Window GetActiveWindow(this IEnumerable<Window> sender)
    {
        var enumerable = sender as Window[] ?? sender.ToArray();
        var activeWindow = enumerable.FirstOrDefault(window => window.IsFocused || window.IsActive)
                           ?? enumerable.Last();

        return activeWindow ?? throw new InvalidOperationException();
    }
    
    public static Window GetWindow()
    {
        try
        {
            if (Application.Current!.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                return desktop.Windows.GetActiveWindow();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        
        return new Window();
    }

    public static Window GetMaxZIndexWindow(this IEnumerable<Window> sender)
    {
        var enumerable = sender.ToList();
        if (enumerable.Any() != true)
            throw new InvalidOperationException();

        var maxZIndex = enumerable.Max(window => window.ZIndex);
        return enumerable.First(window => window.ZIndex == maxZIndex);
    }
    
    
    
}