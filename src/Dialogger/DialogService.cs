using System.Reflection;
using Avalonia;
using Commons.Extensions;
using Dialogger.Commons;
using Splat;

namespace Dialogger;

public class DialogService(Assembly viewsAssembly, Assembly viewModelsAssembly) : IDialogService
{
    public async Task<TResult> ShowDialogAsync<TResult, TParameter>(string viewModelName, TParameter parameter)
        where TResult : DialogResultBase
        where TParameter : NavigationParameterBase
    {
        var window = CreateView<TResult>(viewModelName);
        var viewModel = CreateViewModel<TResult>(viewModelName);
        Bind(window, viewModel);

        await ActivateParameterAsync(viewModel, parameter);

        return await ShowDialogAsync(window);
    }

    public void ShowDialog<TParameter>(string viewModelName, TParameter parameter)
        where TParameter : NavigationParameterBase
    {
        var window = CreateView<DialogResultBase>(viewModelName);
        var viewModel = CreateViewModel<DialogResultBase>(viewModelName);
        Bind(window, viewModel);

        Task.Run(() => ActivateParameterAsync(viewModel, parameter));

        window.Show();
    }

    private static void Bind(IDataContextProvider window, object viewModel)
        => window.DataContext = viewModel;

    private async Task<TResult> ShowDialogAsync<TResult>(DialogWindowBase<TResult> window)
        where TResult : DialogResultBase
    {
        var activeWindow = Extensions.GetWindow();
        var result = await window.ShowDialog<TResult>(activeWindow);

        // ReSharper disable once SuspiciousTypeConversion.Global
        if (window is IDisposable disposable)
        {
            disposable.Dispose();
        }

        return result;
    }
    
    private static async Task ActivateParameterAsync<TResult, TParameter>(DialogViewModelBase<TResult> viewModel,
        TParameter parameter)
        where TResult : DialogResultBase where TParameter : NavigationParameterBase
    {
        switch (viewModel)
        {
            case ParameterizedDialogViewModelBase<TParameter> vm:
                vm.Activate(parameter);
                break;
            case ParameterizedDialogViewModelBase<TResult, TParameter> vm:
                vm.Activate(parameter);
                break;
            case ParameterizedDialogViewModelBaseAsync<TParameter> vm:
                await vm.ActivateAsync(parameter);
                break;
            case ParameterizedDialogViewModelBaseAsync<TResult, TParameter> vm:
                await vm.ActivateAsync(parameter);
                break;
            case DialogViewModelBase:
                break;
            default:
                throw new InvalidOperationException(
                    $"{viewModel.GetType().FullName} doesn't support passing parameters!");
        }
    }

    private DialogViewModelBase<TResult> CreateViewModel<TResult>(string viewModelName)
        where TResult : DialogResultBase
    {
        var viewModelType = GetViewModelType();
        var dialogViewModel = (DialogViewModelBase<TResult>)GetViewModel(viewModelType);

        return dialogViewModel ??
               throw new InvalidOperationException($"View model {viewModelName} can't cast to DialogViewModelBase");

        object GetViewModel(Type type) => Locator.Current.GetRequiredService(type);

        Type GetViewModelType()
        {
            var viewModelTypes = viewModelsAssembly.GetTypes();
            var type = viewModelTypes.SingleOrDefault(t => t.Name == viewModelName);

            return type ?? throw new InvalidOperationException($"View model {viewModelName} was not found!");
        }
    }

    private DialogWindowBase<TResult> CreateView<TResult>(string viewModelName) where TResult : DialogResultBase
    {
        var type = GetViewType();
        var view = Activator.CreateInstance(type)
                   ?? throw new InvalidOperationException($"View for {viewModelName} can't be created");

        return (DialogWindowBase<TResult>)view;

        Type GetViewType()
        {
            var viewName = viewModelName.Replace("ViewModel", string.Empty);
            var viewTypes = viewsAssembly.GetTypes();
            var viewType = viewTypes.SingleOrDefault(t => t.Name == viewName);

            return viewType ?? throw new InvalidOperationException($"View for {viewModelName} was not found!");
        }
    }
}