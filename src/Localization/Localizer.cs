﻿using System.ComponentModel;
using System.Globalization;
using System.Text;
using Avalonia.Platform;
using Newtonsoft.Json;

namespace Localization;

public class Localizer : INotifyPropertyChanged
{
    private const string IndexerName = "Item";
    private const string IndexerArrayName = "Item[]";
    private Dictionary<string, string>? _strings;
    private string _keyPrefix = "";

    public event PropertyChangedEventHandler? PropertyChanged;

    public static Localizer Instance { get; } = new();
    public LanguageType Language { get; private set; }
    public string this[string key]
        => _strings != null && _strings.TryGetValue(key, out var res)
            ? res.Replace("\\n", "\n")
            : $"{_keyPrefix}:{key}";

    public void LoadLanguage(LanguageType languageType)
    {
        Language = languageType;
        _keyPrefix = languageType.ToString();

        var cultureSelected = GetCulture();

        var uri = new Uri($"avares://Localization/Assets/Culture/{cultureSelected}.json");
        Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(cultureSelected);

        if (!AssetLoader.Exists(uri))
            return;

        using var sr = new StreamReader(AssetLoader.Open(uri), Encoding.UTF8);
        _strings = JsonConvert.DeserializeObject<Dictionary<string, string>>(sr.ReadToEnd());
        Invalidate();
    }

    private Localizer() => LoadLanguage(LanguageType.Русский);

    private string GetCulture()
    {
        return Language switch
        {
            LanguageType.Русский => "ru-RU",
            LanguageType.English => "en-US",
            _ => ""
        };
    }

    private void Invalidate()
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(IndexerName));
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(IndexerArrayName));
    }
}