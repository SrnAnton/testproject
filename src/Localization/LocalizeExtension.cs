﻿using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Avalonia.Markup.Xaml.MarkupExtensions;

namespace Localization;

public class LocalizeExtension(string key) : MarkupExtension
{
    public override object ProvideValue(IServiceProvider serviceProvider)
    {
     var binding = new ReflectionBindingExtension($"[{key}]")
        {
            Mode = BindingMode.OneWay,
            Source = Localizer.Instance
        };

        return binding.ProvideValue(serviceProvider);
    }
}