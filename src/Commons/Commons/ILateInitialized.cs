namespace Commons.Commons;


public interface ILateInitialized<out T>
{
    public T Init(object data);
}