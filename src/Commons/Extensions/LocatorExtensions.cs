using Commons.Commons;
using Splat;

namespace Commons.Extensions;

public static class LocatorExtension
{
    public static T To<T>(this object data) where T : ILateInitialized<T>
        => Locator.Current.GetRequiredService<T>().Init(data);

    public static IEnumerable<T> To<T>(this IEnumerable<object> datas) where T : ILateInitialized<T>
        => from data in datas select Locator.Current.GetRequiredService<T>().Init(data);
}