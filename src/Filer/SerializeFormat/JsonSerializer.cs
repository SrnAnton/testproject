using System.Text;
using Newtonsoft.Json;

namespace Filer.SerializeFormat;

public class JsonSerializer: ISerializeMethod
{
    public byte[]  SerializeFrom<T>(T value)
    {
        if (!typeof(T).IsSerializable) 
            return Encoding.Default.GetBytes("");
        
        var json = JsonConvert.SerializeObject(value);
        return Encoding.Default.GetBytes(json);
    }

    public T SerializeTo<T>(byte[] value)
    {
        var json = Encoding.Default.GetString(value);
        if (JsonConvert.DeserializeObject<T>(json) is { } data)
            return data;

        return default!;
    }
}