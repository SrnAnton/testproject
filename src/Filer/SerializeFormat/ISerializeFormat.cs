namespace Filer.SerializeFormat;

public interface ISerializeMethod
{
    public byte[] SerializeFrom<T>(T value);
    public T SerializeTo<T>(byte[]  value);
}