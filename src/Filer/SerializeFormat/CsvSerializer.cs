using System.Text;
using Newtonsoft.Json;

namespace Filer.SerializeFormat;

public class CsvSerializer : ISerializeMethod
{
    private const string _splitSymbol = ";";

    public byte[] SerializeFrom<T>(T value)
    {
        if (!typeof(T).IsSerializable)
            return Encoding.Default.GetBytes("");

        var json = JsonConvert.SerializeObject(value);
        var data = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json);
        var headers = data.First().Keys.ToList();

        var csvBuilder = new StringBuilder();
        csvBuilder.AppendLine(string.Join(_splitSymbol, headers));

        foreach (var record in data)
        {
            var values = headers.Select(header => FormatCsvValue(record.ContainsKey(header) ? record[header] : null));
            csvBuilder.AppendLine(string.Join(_splitSymbol, values));
        }

        var csvString = csvBuilder.ToString();
        return Encoding.Default.GetBytes(csvString);

        string FormatCsvValue(object value) =>
            value switch
            {
                null => string.Empty,
                string => $"\"{value.ToString().Replace("\"", "\"\"")}\"",
                _ => value.ToString()
            };
    }

    public T SerializeTo<T>(byte[] value)
    {
        var csvData = Encoding.Default.GetString(value);
        var jsonString = "{}";
        var lines = csvData.Split(["\r\n", "\r", "\n"], StringSplitOptions.RemoveEmptyEntries);
        if (lines.Length < 2)
        {
            if (JsonConvert.DeserializeObject<T>(jsonString) is { } result)
                return result;
        }

        var headers = lines[0].Split(_splitSymbol);
        var jsonBuilder = new StringBuilder("[");
        for (var i = 1; i < lines.Length; i++)
        {
            var values = lines[i].Split(_splitSymbol);
            if (i > 1)
            {
                jsonBuilder.Append(',');
            }

            jsonBuilder.Append('{');
            for (var j = 0; j < headers.Length; j++)
            {
                var header = headers[j];
                var value1 = j < values.Length ? values[j] : string.Empty;
                if (j > 0)
                {
                    jsonBuilder.Append(',');
                }

                jsonBuilder.Append($"{header}:{value1}");
            }

            jsonBuilder.Append('}');
        }

        jsonBuilder.Append(']');

        jsonString = jsonBuilder.ToString();
        if (JsonConvert.DeserializeObject<T>(jsonString) is { } data)
            return data;


        return default!;
    }
}