﻿using Filer.SerializeFormat;

namespace Filer;

public class FileService<TSerializer>: IFileService 
    where TSerializer : ISerializeMethod , new()
{
    public void Save<T>(string path, T source)
    {
        var stream = new FileStream(path, FileMode.OpenOrCreate);
        var byteData = new TSerializer().SerializeFrom(source);

        stream.WriteAsync(byteData.AsMemory(0, byteData.Length));
        stream.Flush();
    }

    public T Load<T>(string path)
    {
        if(!File.Exists(path))
            return default!;
        
        var stream = new FileStream(path, FileMode.Open);
        var buffer = new byte[stream.Length];
        stream.Seek(0, SeekOrigin.Begin);
        var read = stream.Read(buffer, 0, buffer.Length);
            
        return new TSerializer().SerializeTo<T>(buffer);
    }
}