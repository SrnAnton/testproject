namespace Filer;

public interface IFileService
{
    public void Save<T>(string path, T source);
    public T Load<T>(string path);
}