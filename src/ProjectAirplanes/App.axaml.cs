using System;
using System.Collections.Generic;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Commons.Extensions;
using projectAirplanes.Views;
using Splat;
using ViewModels;
using ViewModels.Interfaces;

namespace projectAirplanes;

public class App : Application
{
    public override void Initialize() => AvaloniaXamlLoader.Load(this);

    public override void OnFrameworkInitializationCompleted()
    {
        DataTemplates.Add(new ViewLocator());
       
        if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
        {
            Queue<Action> actions = new();
            
            actions.Enqueue(Made<MainWindow, IMainWindowViewModel>);

            actions.Dequeue().Invoke();
            return;

            void Made<TWindow, TViewModel>()
                where TWindow : Window, new()
                where TViewModel : IMainWindow
            {
                var window = new TWindow();
                var model = Locator.Current.GetRequiredService<TViewModel>();
                model.SetClosingCallback(() =>
                {
                    if (actions.Count != 0)
                        actions.Dequeue().Invoke();
                    window.Close();
                });
                window.DataContext = model;
                window.Show();
                window.Focus();
                desktop.MainWindow = window;
                model.Initialize();
            }
          
        }
        
        base.OnFrameworkInitializationCompleted();
    }
}