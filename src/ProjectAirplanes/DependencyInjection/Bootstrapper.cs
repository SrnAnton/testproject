using System.Reflection;
using Commons.Extensions;
using Dialogger;
using Filer;
using Filer.SerializeFormat;
using Models;
using projectAirplanes.Views;
using Splat;
using ViewModels.Implementations;
using ViewModels.Implementations.Dialogs;
using ViewModels.Implementations.Dialogs.Models;
using ViewModels.Interfaces;

namespace projectAirplanes.DependencyInjection;

public static class Bootstrapper
{
    public static void Register(IMutableDependencyResolver services, IReadonlyDependencyResolver resolver)
    {
        var views = Assembly.GetAssembly(typeof(MainWindow))!;
        var models = Assembly.GetAssembly(typeof(MainWindowViewModel))!;

        services.RegisterLazySingleton<IDialogService>(() => new DialogService(views, models));
        services.RegisterLazySingleton<IFileService>(() => new FileService<JsonSerializer>());

        services.Register(() => new EntryViewModel());
        services.Register(() => new PassengerViewModel(new Passenger(string.Empty, string.Empty, string.Empty)));
        services.Register(() => new AddNewEntryDialogViewModel(resolver.GetRequiredService<EntryViewModel>()));

        services.RegisterLazySingleton<IMainWindowViewModel>(() =>
            new MainWindowViewModel(
                    resolver.GetRequiredService<IDialogService>(),
                    resolver.GetRequiredService<IFileService>())
                { Version = Program.Version });
    }
}