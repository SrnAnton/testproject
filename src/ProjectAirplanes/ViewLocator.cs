using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Avalonia.Controls;
using Avalonia.Controls.Templates;
using Commons.Commons;

namespace projectAirplanes;

public class ViewLocator : IDataTemplate
{
    public Control Build(object? param)
    {
        Debug.Assert(param != null, nameof(param) + " != null");

        var name = param.GetType().Name;
        var viewType = GetViewType(name);

        return viewType != null
            ? (Control)Activator.CreateInstance(viewType)!
            : new TextBlock { Text = $"View for {param} was not found!" };
    }

    private static Type? GetViewType(string viewModelName)
    {
        var viewsAssembly = Assembly.GetExecutingAssembly();
        var viewTypes = viewsAssembly.GetTypes();
        var viewName = viewModelName.Replace("ViewModel", "View");

        return viewTypes.SingleOrDefault(t => t.Name == viewName);
    }
    
    public bool Match(object? data) => data is ViewModelBase;
}