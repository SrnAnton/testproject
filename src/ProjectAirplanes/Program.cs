﻿using Avalonia;
using Avalonia.ReactiveUI;
using System;
using Logger;
using Logger.Commons;
using projectAirplanes.DependencyInjection;
using Serilog;
using Splat;

namespace projectAirplanes;

static partial class Program
{
    // Initialization code. Don't use any Avalonia, third-party APIs or any
    // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
    // yet and stuff might break.

    public static readonly string Version = GetAppVersion();
    private static readonly string Path = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + $"/.config/{nameof(projectAirplanes)}/Logs/";

    [STAThread]
    public static void Main(string[] args)
    {
        if (args.Length != 0)
        {
            Check(args);
            return;
        }

        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .Enrich.FromLogContext()
            .WriteTo.Console(outputTemplate: LoggerConst.Format)
            .WriteTo.LogFile(Path)
            .CreateLogger();
        
        LogRegister.Information("LogRegister is initialized!");

        Bootstrapper.Register(Locator.CurrentMutable, Locator.Current);
        BuildAvaloniaApp().StartWithClassicDesktopLifetime(args);
    }

    // Avalonia configuration, don't remove; also used by visual designer.
    public static AppBuilder BuildAvaloniaApp()
    {
        return AppBuilder
            .Configure<App>()
            .UsePlatformDetect()
            .WithInterFont()
            .LogToTrace()
            .UseReactiveUI();
    }
}