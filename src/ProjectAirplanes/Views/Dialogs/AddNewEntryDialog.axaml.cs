using Dialogger.Commons;
using ViewModels.Implementations.Dialogs;

namespace ProjectAirplanes.Views.Dialogs;

public partial class AddNewEntryDialog : DialogWindowBase<NewEntryDialogResult>
{
    public AddNewEntryDialog() => InitializeComponent();
}