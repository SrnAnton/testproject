using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace projectAirplanes;

static partial class Program
{
    private static void Check(IReadOnlyList<string> args)
    {
        if(args.Any())
            return;
    }

    private static string GetAppVersion()
    {
        try
        {
            string? informationalVersion = Assembly.GetExecutingAssembly()
                .GetCustomAttribute<AssemblyInformationalVersionAttribute>()?
                .InformationalVersion;
            var assemblyVersion = Assembly.GetExecutingAssembly().GetName().Version;

            if (informationalVersion != null)
                return informationalVersion;
            return assemblyVersion != null ? assemblyVersion.ToString() : "";
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }
}