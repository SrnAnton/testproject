namespace ViewModels;

public interface IMainWindow
{
    public void Initialize();
    public void SetClosingCallback(Action action);
}