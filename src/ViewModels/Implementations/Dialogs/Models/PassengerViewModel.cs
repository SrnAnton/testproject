using Commons.Commons;
using Models;

namespace ViewModels.Implementations.Dialogs.Models;

public class PassengerViewModel(Passenger passenger) : ViewModelBase, ILateInitialized<PassengerViewModel>
{
    public string Name => passenger.Name;
    public string MiddleName => passenger.MiddleName;
    public string Surname => passenger.Surname;
    public PassengerViewModel Init(object data)
    {
        if (data is not Passenger newPassenger)
            return this;

        passenger = newPassenger;
        return this;
    }

    public Passenger GetModel() => passenger;
}