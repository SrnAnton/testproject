using System.Collections.ObjectModel;
using Commons.Commons;
using Commons.Extensions;
using DynamicData;
using Models;
using ReactiveUI.Fody.Helpers;

namespace ViewModels.Implementations.Dialogs.Models;

public class EntryViewModel : ViewModelBase, ILateInitialized<EntryViewModel>
{
    [Reactive] public string Flight { get; set; } = string.Empty;
    [Reactive] public DateTime DepartureTime { get; set; } = DateTime.Now;

    public ObservableCollection<PassengerViewModel> Passengers { get; } = [];

    public void AddPassenger(PassengerViewModel passenger) 
        => Passengers.Add(passenger);

    public EntryViewModel Init(object data)
    {
        if (data is not EntryModel entry)
            return this;

        Flight = entry.Flight;
        DepartureTime = entry.DepartureTime;
        Passengers.Clear();
        
        Passengers.AddRange(entry.Passengers.To<PassengerViewModel>());
        return this;
    }

    public EntryModel GetModel()
    {
        var passengers = Passengers.Select(passenger => passenger.GetModel()).ToList();
        var model = new EntryModel {Flight = Flight, DepartureTime = DepartureTime};
        model.Passengers.AddRange(passengers);
        return model;
    }
}