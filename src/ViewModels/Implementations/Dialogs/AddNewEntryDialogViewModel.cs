using System.Windows.Input;
using Dialogger;
using Dialogger.Commons;
using Models;
using ReactiveUI;
using ViewModels.Implementations.Dialogs.Models;

namespace ViewModels.Implementations.Dialogs;

public class NewEntryDialogResult(EntryViewModel entry) : DialogResultBase
{
    public EntryViewModel Entry { get; } = entry;
}

public class AddNewEntryDialogViewModel :
    ParameterizedDialogViewModelBaseAsync<NewEntryDialogResult, NavigationParameterBase>
{
    public EntryViewModel Entry { get; }
    
    public ICommand AddCommand { get; }
    public ICommand CancelCommand { get; }
    public ICommand AddNewPassengerCommand { get; }
    
    public string Name { get; set; } = string.Empty;

    public string MiddleName { get; set; } = string.Empty;

    public string Surname { get; set; } = string.Empty;
 

    public AddNewEntryDialogViewModel(EntryViewModel entry)
    {
        Entry = entry;
        AddCommand = ReactiveCommand.Create(Add);
        CancelCommand = ReactiveCommand.Create(Cancel);
        AddNewPassengerCommand = ReactiveCommand.Create(AddNewPassenger);
    }

    private void AddNewPassenger()
    {
        Entry.AddPassenger(new PassengerViewModel(new Passenger(Name, MiddleName, Surname)));
    }

    public override Task ActivateAsync(NavigationParameterBase parameter, CancellationToken cancellationToken = default)
        => Task.CompletedTask;

    private void Cancel() => Close(null!);
    private void Add() => Close(new NewEntryDialogResult(Entry));
}