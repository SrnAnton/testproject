﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Commons.Commons;
using Commons.Extensions;
using Dialogger;
using DynamicData;
using Filer;
using Models;
using ReactiveUI;
using ViewModels.Implementations.Dialogs;
using ViewModels.Implementations.Dialogs.Models;
using ViewModels.Interfaces;

namespace ViewModels.Implementations;

public sealed class MainWindowViewModel : ViewModelBase, IMainWindowViewModel
{
    private readonly IDialogService _dialogService;
    private readonly IFileService _fileService;
    public string Version { get; init; } = string.Empty;
    public ObservableCollection<EntryViewModel> Items { get; } = new();

    public string Path { get; set; } = $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}/Entries";

    public ICommand SaveFileCommand { get; }
    public ICommand LoadFileCommand { get; }
    public ICommand AddNewCommand { get; }

    public MainWindowViewModel(IDialogService dialogService, IFileService fileService)
    {
        _dialogService = dialogService;
        _fileService = fileService;
        SaveFileCommand = ReactiveCommand.Create(SaveFile);
        LoadFileCommand = ReactiveCommand.Create(LoadFile);
        AddNewCommand = ReactiveCommand.Create(AddNew);
    }

    public void Initialize()
    {
    }

    public void SetClosingCallback(Action action)
    {
    }

    private void LoadFile()
    {
        var entries = _fileService.Load<List<EntryModel>>(Path);
        Items.Clear();
        Items.AddRange(entries.To<EntryViewModel>());
    }

    private async void AddNew()
    {
        var result = await _dialogService.ShowDialogAsync<NewEntryDialogResult, NavigationParameterBase>
            (nameof(AddNewEntryDialogViewModel), new NavigationParameterBase());

        if (result is null)
            return;

        Items.Add(result.Entry);
    }

    private void SaveFile()
    {
        var entries = Items.Select(entry => entry.GetModel()).ToList();
        _fileService.Save(Path, entries);
    }
}